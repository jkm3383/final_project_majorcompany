import { useNavigate } from "react-router-dom";
import MemberListCSS from './MemberList.module.css';

function MemberList({ prop : {memberCode, memberName, memberBirth, memberTel, memberAddress, memberJoin, memberLeave, memberEnt, memberRemanet,
    memberGrade, memberDep, memberRtPayment, memberRtStatus, memberId, memberEmail, memberSalaryBonus}}) {
  
  const navigate = useNavigate();

  const onClickFreeHandler = (memberCode) => {
    navigate(`/management/member/list/${memberCode}`, { replace: true });
  }

  return (
    <div style={ memberEnt == "Y" ? { backgroundColor: '#d3d3d3'} : null}>
      <hr width= "1400px"></hr> 
      <div style={{width:'1400px'}} onClick={ () => onClickFreeHandler(memberCode) }>
        <div className={ MemberListCSS.name}>{memberName}</div>
        <div className={ MemberListCSS.item }>
        <span>생일 :&nbsp;{ memberBirth }</span>&nbsp;&nbsp;&nbsp;
        <span>전화번호 :&nbsp;{ memberTel }</span>&nbsp;&nbsp;&nbsp;
        <span>주소 :&nbsp;{ memberAddress }</span>&nbsp;&nbsp;&nbsp;
        <span>입사일자 :&nbsp;{ memberJoin }</span>&nbsp;&nbsp;&nbsp;
        <span>퇴사일자 :&nbsp;{ memberLeave }</span>&nbsp;&nbsp;&nbsp;
        <span>퇴직여부 :&nbsp;{ memberEnt }</span>&nbsp;&nbsp;&nbsp;
        <span>휴가잔여일수 :&nbsp;{ memberRemanet }</span>&nbsp;&nbsp;&nbsp;
        <span>직급 :&nbsp;{ memberGrade }</span>&nbsp;&nbsp;&nbsp;
        <span>부서 :&nbsp;{ memberDep }</span>&nbsp;&nbsp;&nbsp;
        <span>퇴직금실지급액 :&nbsp;{ memberRtPayment }</span>&nbsp;&nbsp;&nbsp;
        <span>퇴직금실지급액 :&nbsp;{ memberRtStatus }</span>&nbsp;&nbsp;&nbsp;
        <span>아이디 :&nbsp;{ memberId }</span>&nbsp;&nbsp;&nbsp;
        <span>이메일 :&nbsp;{ memberEmail }</span>&nbsp;&nbsp;&nbsp;
        <span>급여 :&nbsp;{ memberSalaryBonus }</span>&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    <hr width= "800px"></hr>
    </div>
  );
}
export default MemberList;