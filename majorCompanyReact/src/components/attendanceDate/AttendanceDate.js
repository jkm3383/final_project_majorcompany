import AttendanceDateCSS from './AttendanceDate.module.css';
import { NavLink } from 'react-router-dom';

function AttendanceDate({prop : {start, end, attendanceHour, attendanceOvertime,
                                 member, attendanceDate, title}}) {

  return (
    <span className={ AttendanceDateCSS.registerDiv } style={{marginTop:'10px'}}>
        <NavLink to={"/management/manageAttendance/" + member.memberId}>
        <ul>
            <li className={ AttendanceDateCSS.registerDiv2 }>근무자 ID : {member.memberId}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>근무일자 : {attendanceDate}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>근무상태 : {title}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>출근시각 : {start}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>퇴근시각 : {end}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>근무시간 : {attendanceHour}</li> 
            <li className={ AttendanceDateCSS.registerDiv2 }>초과 근무 시간 : {attendanceOvertime}</li> 
        </ul>
        </NavLink>
    </span>
  );
}
export default AttendanceDate;