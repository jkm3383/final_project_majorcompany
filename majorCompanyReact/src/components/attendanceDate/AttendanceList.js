import AttendanceListCSS from './AttendanceList.module.css';
import { NavLink } from 'react-router-dom';

function AttendanceList({prop : {id, title, attendanceDate, start, end, attendanceHour, attendanceOvertime}}) {

  return (
    <>
      <span className={ AttendanceListCSS.registerDiv } style={{marginTop:'10px'}}>
        <NavLink to={"/management/manageAttendance/list/" + id}>
          <li className={ AttendanceListCSS.registerDiv2 }>근무일자 : {attendanceDate}</li> 
          <li className={ AttendanceListCSS.registerDiv2 }>근무상태 : {title}</li> 
          <li className={ AttendanceListCSS.registerDiv2 }>출근시각 : {start}</li> 
          <li className={ AttendanceListCSS.registerDiv2 }>퇴근시각 : {end}</li> 
          <li className={ AttendanceListCSS.registerDiv2 }>근무시간 : {attendanceHour}</li> 
          <li className={ AttendanceListCSS.registerDiv2 }>초과근무시간 : {attendanceOvertime}</li> 
        </NavLink>
      </span>
    </>
  );
}
export default AttendanceList;