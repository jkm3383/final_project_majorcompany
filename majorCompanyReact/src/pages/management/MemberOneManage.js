import React from 'react';
import { useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { decodeJwt } from '../../utils/tokenUtils';
import { callGetEmpOneAPI,
         callPutEmpOneAPI } from '../../apis/MemberManageAPICalls';
import MemberOneManageCSS from './MemberOneManage.module.css';

function MemberOneManage() {

  const params = useParams();  
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const member = useSelector(state => state.memberManagementReducer);  
  const token = decodeJwt(window.localStorage.getItem("accessToken"));   
  const [form, setForm] = useState({});
  const [modifyMode, setModifyMode] = useState(false);
  const [image, setImage] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const imageInput = useRef();

  console.log("member : {} ", member && member);
 
  useEffect(        
    () => {
        console.log('memberCode : ', params.memberCode);

        dispatch(callGetEmpOneAPI({   
            memberCode: params.memberCode
        }));                     
    }
    ,[]
  );

  useEffect(() => {
        
    if(image){
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
            const { result } = e.target;
            if( result ){
                setImageUrl(result);
            }
        }
        fileReader.readAsDataURL(image);
    }
  },
  [image]);

  const onChangeImageUpload = (e) => {
    console.log(e.target.files[0]);
    const image = e.target.files[0];

    setImage(image);
  };

  const onClickImageUpload = () => {
      if(modifyMode){
          imageInput.current.click();
      }
  }

  const onClickModifyModeHandler = () => {
    setModifyMode(true);
    setForm({
      memberCode: member.memberCode,
      memberId: member.memberId,
      memberName: member.memberName,
      memberBirth: member.memberBirth,
      memberTel: member.memberTel,
      memberAddress: member.memberAddress,
      memberLeave: member.memberLeave,
      memberEnt: member.memberEnt,
      memberRemanet: member.memberRemanet,
      memberDep: member.memberDep,
      memberRtPayment: member.memberRtPayment,
      memberRtStatus: member.memberRtStatus,
      memberEmail: member.memberEmail,
      memberSalaryBonus: member.memberSalaryBonus,
      memberGrade: member.memberGrade
    });
  }

  const onChangeHandler = (e) => {
    setForm({
        ...form,
        [e.target.name]: e.target.value
    });
  };

  const onClickMemberUpdateHandler = () => {

    console.log('[MemberUpdate] onClickMemberUpdateHandler');

    const formData = new FormData();
    formData.append("memberCode", form.memberCode);
    formData.append("memberId", form.memberId);
    formData.append("memberName", form.memberName);
    formData.append("memberBirth", form.memberBirth);
    formData.append("memberTel", form.memberTel);
    formData.append("memberAddress", form.memberAddress);
    formData.append("memberLeave", form.memberLeave);
    formData.append("memberEnt", form.memberEnt);
    formData.append("memberRemanet", form.memberRemanet);
    formData.append("memberDep", form.memberDep);
    formData.append("memberRtPayment", form.memberRtPayment);
    formData.append("memberRtStatus", form.memberRtStatus);
    formData.append("memberEmail", form.memberEmail);
    formData.append("memberSalaryBonus", form.memberSalaryBonus);
    formData.append("memberGrade", form.memberGrade);

    if(image){
      formData.append("memberImage", image);
    }
    console.log("formData?", form);

    dispatch(callPutEmpOneAPI({	
        form: formData
    }));         
    alert('저장되었습니다..잠시만 기다려 주세요.');
    setTimeout(() => {
        alert('사원 정보를 수정했습니다.');
        navigate('/management/member/list', { replace: true });
        window.location.reload();
    }, 2000);
};

  const onClickAttendanceHandler = () => {
        
    navigate(`/management/manageAttendance/${member.memberId}`, {replace: true});
  }

  const onClickBackHandler = () => {
        
    navigate(-1);
  }

  return (
    <>
      {member &&
      <div 
        className={ MemberOneManageCSS.backgroundDiv}
        style={ { backgroundColor: 'white' } }
      >
      <div className={ MemberOneManageCSS.profileDiv }>
        {!modifyMode && <h1>사원 정보 조회</h1>}
          {modifyMode && <h1>사원 정보 수정</h1>}
            <div className={ MemberOneManageCSS.productInfoDiv }>
                      <div className={ MemberOneManageCSS.productImageDiv }>
                      {!modifyMode &&
                          <img src={ member.memberFile } className={ MemberOneManageCSS.productImage } style={{width:'150px'}} alt="사원 사진" />
                      }
                      { modifyMode && member && <img 
                          className={ MemberOneManageCSS.productImage } 
                          src={ (imageUrl == null) ? member.memberFile : imageUrl } 
                          alt="preview"
                      />}
                          <input                
                              style={ { display: 'none' }}
                              type="file"
                              name='memberImage' 
                              accept='image/jpg,image/png,image/jpeg,image/gif'
                              onChange={ onChangeImageUpload }
                              ref={ imageInput }                            
                          />
                          <button 
                              className={ MemberOneManageCSS.productImageButton }
                              onClick={ onClickImageUpload }   
                              style={ !modifyMode ? { backgroundColor: 'gray'} : null} 
                          >
                              이미지 업로드
                          </button>
                      </div>
                  </div>
                  <span>
                    <>아이디 : </>
                    <input 
                      name='memberId'
                      placeholder='아이디'
                      value={((!modifyMode ? member.memberId : form.memberId) || '')}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>이름 : </>
                    <input 
                      name='memberName'
                      placeholder='이름'
                      value={ (!modifyMode ? member.memberName : form.memberName) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>직급 : </>
                    <input 
                      name='memberGrade'
                      placeholder='직급'
                      value={ (!modifyMode ? member.memberGrade : form.memberGrade) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>부서 : </>
                    <input 
                      name='memberDep'
                      placeholder='부서'
                      value={ (!modifyMode ? member.memberDep : form.memberDep) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>이메일 : </>
                    <input 
                      name='memberEmail'
                      placeholder='이메일'
                      value={ (!modifyMode ? member.memberEmail : form.memberEmail) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>전화번호 : </>
                    <input 
                      name='memberTel'
                      placeholder='전화번호'
                      value={ (!modifyMode ? member.memberTel : form.memberTel) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>주소 : </>  
                    <input 
                      name='memberAddress'
                      placeholder='주소'
                      value={ (!modifyMode ? member.memberAddress : form.memberAddress) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>생년월일 : </>
                    <input 
                      name='memberBirth'
                      placeholder='생년월일'
                      value={ (!modifyMode ? member.memberBirth : form.memberBirth) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>입사일자 : </>
                    <input 
                      name='memberJoin'
                      value={member.memberJoin || ''}
                      style={ modifyMode ? { backgroundColor: '#d3d3d3'} : null}
                      readOnly={ true }
                    />
                  </span>
                  <span>
                    <>퇴직 여부 : </>
                    <input 
                      name='memberEnt'
                      placeholder='퇴직 여부'
                      value={ (!modifyMode ? member.memberEnt : form.memberEnt) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>퇴사 일자 : </>
                    <input 
                      name='memberLeave'
                      placeholder='퇴사 일자'
                      value={ (!modifyMode ? member.memberLeave : form.memberLeave) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>휴가 잔여 일수 : </>
                    <input 
                      name='memberRemanet'
                      placeholder='휴가 잔여 일수'
                      value={ (!modifyMode ? member.memberRemanet : form.memberRemanet) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>퇴직금 실지급액 : </>
                    <input 
                      name='memberRtPayment'
                      placeholder='퇴직금 실지급액'
                      value={ (!modifyMode ? member.memberRtPayment : form.memberRtPayment) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>퇴직금 지급 여부 : </>
                    <input 
                      name='memberRtStatu'
                      placeholder='퇴직금 지급 여부'
                      value={ (!modifyMode ? member.memberRtStatu : form.memberRtStatu) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
                  <span>
                    <>급여 : </>
                    <input 
                      name='memberSalaryBonus'
                      placeholder='급여'
                      value={ (!modifyMode ? member.memberSalaryBonus : form.memberSalaryBonus) || ''}
                      onChange={ onChangeHandler }
                      readOnly={ modifyMode ? false : true }
                    />
                  </span>
        <div className={ MemberOneManageCSS.productButtonDiv }>
              {!modifyMode &&
                  <button       
                  onClick={ onClickModifyModeHandler }             
                  >
                  수정모드
                  </button>
              }
              {modifyMode &&
                  <button       
                  onClick={ onClickMemberUpdateHandler }             
                  >
                  수정 저장하기
                  </button>
              }
      </div>
      </div>
      &nbsp;
          <button
          style={ { border: 'none', margin: 0, fontSize: '18px', height: '25px' } }
          onClick = { onClickAttendanceHandler }
          >
          사원 근태 조회
          </button>
          &nbsp;&nbsp;
          <button
              style={ { border: 'none', margin: 0, fontSize: '18px', height: '25px' } }
              onClick = { onClickBackHandler }
          >
          돌아가기
          </button>
      </div>
      }
    </>
  );
}

export default MemberOneManage;